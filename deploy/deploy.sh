#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="/var/www/html/nodeTestApp"

# Building React output
npm install
npm run build

echo "Deploying to ${DEPLOY_SERVER}"
scp -r build/* ubuntu@${DEPLOY_SERVER}:${SERVER_FOLDER}

echo "Finished copying the build files"
